# liste-instances

Une liste des instances des différents services.

## Contribuer

Pour ajouter votre instance, merci de faire une MR dans le fichier yaml du service, dans l'ordre alphabétique.

## Jitsi

Si vous avez fait l'install par défault, alors vous avez un JVB et pas de TURN.